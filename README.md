# Readme

This repository contains some tutorial code for async runtimes in Rust that I found and did.
It also contains some links to the topic directly in this README.

## Links

* [What is an async runtime](https://www.ncameron.org/blog/what-is-an-async-runtime/)
* [embassy](https://embassy.dev/)
* [smol](https://github.com/smol-rs/smol)
* [tokio](https://tokio.rs/)
* [async-std](https://async.rs/)
* [asynchronous programming in Rust](https://rust-lang.github.io/async-book/)
* [A practical guide to async in Rust](https://blog.logrocket.com/a-practical-guide-to-async-in-rust/)
* [async programming in Rust with async-std](https://book.async.rs/)
* [Tokio tutorial](https://tokio.rs/tokio/tutorial)
* [write code using async await in Rust](https://developerlife.com/2022/03/12/rust-tokio/)
* [whorl](https://github.com/mgattozzi/whorl)
* [Rust async runtime](https://mirrors.cnnic.cn/tuna/tunight/2020-05-09-rust-async-runtime/slides.pdf)
* [Write an async runtime - Basic concept](https://blog.hedwing.dev/async-rust/rio/write_runtime/01/)

## License

All code is licensed under [MIT](./LICENSE).
